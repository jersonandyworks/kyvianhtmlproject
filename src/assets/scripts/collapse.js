$(document).ready(() => {
  /* MEDIA QUERY*/
  function mediaQuery(x) {
    if (x.matches) {
      // If media query matches
      $(".collapse-content-funnel").hide();
      $(".mobile-content-funnel").hide();

      $(".collapse-content-membership").hide();
      $(".mobile-content-membership").hide();

      $(".collapse-content-products").hide();
      $(".mobile-content-products").hide();

      $(".collapse-content-lists").hide();
      $(".mobile-content-lists").hide();

      $(".collapse-content-blog").hide();
      $(".mobile-content-blog").hide();

      $(".collapse-content-affiliate").hide();
      $(".mobile-content-affiliate").hide();

      $(".collapse-content-support").hide();
      $(".mobile-content-support").hide();
      $(".info").hide();

      $(".collapse-header-general").click(e => {
        e.preventDefault();
        $(".collapse-header-general > span").toggleClass("rotateText");
        $(".collapse-content-general").toggle();
        $(".mobile-content-general").toggle();
      });

      $(".collapse-header-funnel").click(e => {
        e.preventDefault();
        $(".collapse-header-funnel > span").toggleClass("rotateText");
        $(".collapse-content-funnel").toggle();
        $(".mobile-content-funnel").toggle();
      });

      $(".collapse-header-products").click(e => {
        e.preventDefault();
        $(".collapse-header-products > span").toggleClass("rotateText");
        $(".collapse-content-products").toggle();
        $(".mobile-content-products").toggle();
        $(".mobile-content-products").toggleClass("make-table-row-show");
      });

      $(".collapse-header-membership").click(e => {
        e.preventDefault();
        $(".collapse-header-membership > span").toggleClass("rotateText");
        $(".collapse-content-membership").toggle();
        $(".mobile-content-membership").toggle();
        $(".mobile-content-membership").toggleClass("make-table-row-show");
      });

      $(".collapse-header-lists").click(e => {
        e.preventDefault();
        $(".collapse-header-lists > span").toggleClass("rotateText");
        $(".collapse-content-lists").toggle();
        $(".mobile-content-lists").toggle();
        $(".mobile-content-lists").toggleClass("make-table-row-show");
      });

      $(".collapse-header-blog").click(e => {
        e.preventDefault();
        $(".collapse-header-blog > span").toggleClass("rotateText");
        $(".collapse-content-blog").toggle();
        $(".mobile-content-blog").toggle();
        $(".mobile-content-blog").toggleClass("make-table-row-show");
      });

      $(".collapse-header-affiliate").click(e => {
        e.preventDefault();
        $(".collapse-header-affiliate > span").toggleClass("rotateText");
        $(".collapse-content-affiliate").toggle();
        $(".mobile-content-affiliate").toggle();
        $(".mobile-content-affiliate").toggleClass("make-table-row-show");
      });

      $(".collapse-header-support").click(e => {
        e.preventDefault();
        $(".collapse-header-support > span").toggleClass("rotateText");
        $(".collapse-content-support").toggle();
        $(".mobile-content-support").toggle();
        $(".mobile-content-support").toggleClass("make-table-row-show");
      });
    } else {
      $(".collapse-content-funnel").hide();
      $(".mobile-content-funnel").hide();

      $(".collapse-content-membership").hide();
      $(".mobile-content-membership").hide();

      $(".collapse-content-products").hide();
      $(".mobile-content-products").hide();

      $(".collapse-content-lists").hide();
      $(".mobile-content-lists").hide();

      $(".collapse-content-blog").hide();
      $(".mobile-content-blog").hide();

      $(".collapse-content-affiliate").hide();
      $(".mobile-content-affiliate").hide();

      $(".collapse-content-support").hide();
      $(".mobile-content-support").hide();

      $("#general .question").mouseover(() => {
        $(".info").removeClass("animated fadeOutRight");
        $(".info").addClass("animated fadeInRight");
        $(".info").show();
      });

      $("#general .question").mouseout(() => {
        $(".info").removeClass("animated fadeInRight");
        $(".info").addClass("animated fadeOutRight");
        // $(".info").hide();
      });
      $(".info").hide();

      $(".collapse-header-general").click(e => {
        e.preventDefault();
        $(".collapse-header-general > span").toggleClass("rotateText");
        $(".collapse-content-general").toggle();
      });

      $(".collapse-header-funnel").click(e => {
        e.preventDefault();
        $(".collapse-header-funnel > span").toggleClass("rotateText");
        $(".collapse-content-funnel").toggle();
      });

      $(".collapse-header-products").click(e => {
        e.preventDefault();
        $(".collapse-header-products > span").toggleClass("rotateText");
        $(".collapse-content-products").toggle();
      });

      $(".collapse-header-membership").click(e => {
        e.preventDefault();
        $(".collapse-header-membership > span").toggleClass("rotateText");
        $(".collapse-content-membership").toggle();
      });

      $(".collapse-header-lists").click(e => {
        e.preventDefault();
        $(".collapse-header-lists > span").toggleClass("rotateText");
        $(".collapse-content-lists").toggle();
      });

      $(".collapse-header-blog").click(e => {
        e.preventDefault();
        $(".collapse-header-blog > span").toggleClass("rotateText");
        $(".collapse-content-blog").toggle();
      });

      $(".collapse-header-affiliate").click(e => {
        e.preventDefault();
        $(".collapse-header-affiliate > span").toggleClass("rotateText");
        $(".collapse-content-affiliate").toggle();
      });

      $(".collapse-header-support").click(e => {
        e.preventDefault();
        $(".collapse-header-support > span").toggleClass("rotateText");
        $(".collapse-content-support").toggle();
      });
    }
  }
  var x = window.matchMedia("(max-width: 700px)");
  mediaQuery(x);
  x.addListener(mediaQuery);
});
