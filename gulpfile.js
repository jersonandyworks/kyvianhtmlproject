const babel = require("gulp-babel");
const concat = require("gulp-concat");
const htmlmin = require("gulp-htmlmin");
const gulp = require("gulp");
const rename = require("gulp-rename");
const terser = require("gulp-terser");
const prettyUrl = require("gulp-pretty-url");
const injectPartials = require("gulp-inject-partials");
const imagemin = require("gulp-imagemin");
const cleanCSS = require("gulp-clean-css");
const sass = require("gulp-sass");
sass.compiler = require("node-sass");
const browserSync = require("browser-sync").create();
const handlebars = require("gulp-compile-handlebars");
const data = require("gulp-data");
const fs = require("fs");
async function browserify() {
  await browserSync.init({
    server: {
      baseDir: "./_dist/"
    }
  });

  await gulp
    .watch("./src/assets/scripts/*.js", minify)
    .on("change", browserSync.reload);
  await gulp
    .watch("./src/assets/styles/*.scss", compileCSS)
    .on("change", browserSync.reload);
  await gulp
    .watch("./src/**/*.html", pretifyHtml)
    .on("change", browserSync.reload);
  await gulp
    .watch("./src/**/*.hbs", pretifyHtml)
    .on("change", browserSync.reload);
  await gulp
    .watch("./src/assets/images/*", compressImages)
    .on("change", browserSync.reload);
  await gulp
    .watch("./src/**/*.handlebars", pretifyHtml)
    .on("change", browserSync.reload);
  await gulp
    .watch("./src/**/*.json", pretifyHtml)
    .on("change", browserSync.reload);
}
async function minify() {
  await gulp
    .src("src/assets/scripts/**/*.js")
    .pipe(gulp.src("./assets/scripts/**/*.js"))
    .pipe(babel())
    .pipe(concat("all"))
    .pipe(terser())
    .pipe(rename({ extname: ".min.js" }))
    .pipe(gulp.dest("./_dist/assets/scripts/"));
}

async function pretifyHtml() {
  /* const templateData = JSON.parse(
    fs.readFileSync("./src/assets/data/menu.json")
  );
  const options = {
    ignorePartials: true, //ignores the unknown footer2 partial in the handlebars template, defaults to false
    partials: {
      footer: "<footer>the end</footer>"
    },
    batch: ["./src/partial"],
    helpers: {
      capitals: function(str) {
        return str.toUpperCase();
      }
    }
  };*/

  await gulp
    .src("./src/index.handlebars")
    // .pipe(handlebars(templateData, options))
    .pipe(injectPartials())
    .pipe(prettyUrl())
    .pipe(htmlmin({ collapseWhitespace: true }))
    .pipe(gulp.dest("./_dist/"));
}

async function templatify() {
  /*const templateData = JSON.parse(
    fs.readFileSync("./src/assets/data/menu.json")
  );
  const options = {
    ignorePartials: true, //ignores the unknown footer2 partial in the handlebars template, defaults to false
    partials: {
      footer: "<footer>the end</footer>"
    },
    batch: ["./src/partial"],
    helpers: {
      capitals: function(str) {
        return str.toUpperCase();
      }
    }
  };*/

  return (
    gulp
      .src("./src/index.handlebars")
      // .pipe(handlebars(templateData, options))
      .pipe(rename("index.html"))
      .pipe(gulp.dest("./_dist"))
  );
}

async function compressImages() {
  await gulp
    .src("./src/assets/images/*")
    .pipe(imagemin())
    .pipe(gulp.dest("./_dist/assets/images/"));
}
async function compileCSS() {
  await gulp
    .src("./src/assets/styles/**/*.scss")
    .pipe(sass.sync().on("error", sass.logError))
    .pipe(gulp.src("./src/assets/**/*.css"))
    .pipe(concat("all"))
    .pipe(cleanCSS({ compatibility: "ie8" }))
    .pipe(rename({ extname: ".min.css" }))
    .pipe(gulp.dest("./_dist/assets/styles/"));
}

exports.default = gulp.series(
  minify,
  compileCSS,
  compressImages,
  templatify,
  pretifyHtml,
  browserify
);
